﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    class Game
    {
        private static readonly Game instance = new Game();
        public int Score { get; set; }
        private Snake Snake { get; set; }
        public Apple Apple { get; private set; }

        public static Game Instance
        {
            get { return instance; }
        }

        private Game()
        {
            Console.WindowHeight = Constants.SCREEN_HEIGHT;
            Console.WindowWidth = Constants.SCREEN_WIDTH;
            Console.BufferHeight = Constants.SCREEN_HEIGHT;
            Console.BufferWidth = Constants.SCREEN_WIDTH;

            Score = 0;
        }
        public void Start()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Snake = new Snake(new Utils.Point(Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2));
            Apple = new Apple();
            for (int i = 1; ; i++)
            {
                Snake.Update();
                Console.Clear();
                Console.SetCursorPosition(0, 0);
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("Score: {0}", Score);
                Snake.Draw();
                Apple.Draw();
                System.Threading.Thread.Sleep(50);
            }
        }
    }
}
