﻿using System;
using System.Collections.Generic;

namespace Snake
{
    class Snake
    {
        List<Utils.Point> Body { get; set; }
        ConsoleColor Color { get; set; }
        Direction Direction { get; set; }
        public Snake(Utils.Point start_position)
        {
            Body = new List<Utils.Point>();
            Body.Add(start_position);
            Direction = Direction.Up;
            Color = ConsoleColor.Green;
        }
        public Snake(Utils.Point start_position, ConsoleColor color)
            : this(start_position)
        {
            Color = color;
        }
        private void GetDirection()
        {
            if(Console.KeyAvailable)
            {
                var key = Console.ReadKey(true).Key;
                if (key == ConsoleKey.W && Direction != Direction.Down)
                    Direction = Direction.Up;
                if (key == ConsoleKey.D && Direction != Direction.Left)
                    Direction = Direction.Right;
                if (key == ConsoleKey.S && Direction != Direction.Up)
                    Direction = Direction.Down;
                if (key == ConsoleKey.A && Direction != Direction.Right)
                    Direction = Direction.Left;
            }
        }
        public void Update()
        {
            GetDirection();
            if (Direction == Direction.Up) Body.Add(Body[Body.Count - 1] - new Utils.Point(0, 1));
            if (Direction == Direction.Right) Body.Add(Body[Body.Count - 1] + new Utils.Point(1, 0));
            if (Direction == Direction.Down) Body.Add(Body[Body.Count - 1] + new Utils.Point(0, 1));
            if (Direction == Direction.Left) Body.Add(Body[Body.Count - 1] - new Utils.Point(1, 0));
            if (Body.FindAll(v => v.X == Body[Body.Count - 1].X && v.Y == Body[Body.Count - 1].Y).Count > 1)
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                Console.Clear();
                Console.SetCursorPosition(Constants.SCREEN_WIDTH / 2 - 8, Constants.SCREEN_HEIGHT/2-1);
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write("GAME OVER, BITCH");
                Console.SetCursorPosition(Constants.SCREEN_WIDTH / 2 - 5, Constants.SCREEN_HEIGHT/2+1);
                Console.Write("SCORE: {0}",Game.Instance.Score);
                System.Threading.Thread.Sleep(100);
                Console.ReadKey();
                Game.Instance.Start();
            }
            if (Body[Body.Count - 1].X == Game.Instance.Apple.Position.X && Body[Body.Count - 1].Y == Game.Instance.Apple.Position.Y)
            {
                Game.Instance.Apple.Move();
                Game.Instance.Score += 10;
            } 
            else
            {
                Body.RemoveAt(0);
            }  
        }
        public void Draw()
        {
            foreach (var p in Body)
            {
                Console.SetCursorPosition(p.X, p.Y);
                Console.ForegroundColor = Color;
                Console.Write(Constants.SNAKE_BODY_SYMBOL);
            }
        }
    }
}
