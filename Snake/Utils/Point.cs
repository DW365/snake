﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake.Utils
{
    class Point
    {
        private int x;
        private int y;
        public int X
        {
            get 
            {
                return x;
            }
            set
            {
                if (value >= 0)
                    x = value % Constants.SCREEN_WIDTH;
                else
                    x = Constants.SCREEN_WIDTH + value % Constants.SCREEN_WIDTH;
            }
        }
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                if (value >= 0)
                    y =  value % Constants.SCREEN_HEIGHT;
                else
                    y = Constants.SCREEN_HEIGHT + value % Constants.SCREEN_HEIGHT;
            }
        }
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
        public static Point operator+ (Point a, Point b)
        {
            return new Point(a.X + b.X, a.Y + b.Y);
        }
        public static Point operator -(Point a, Point b)
        {
            return new Point(a.X - b.X, a.Y - b.Y);
        }
    }
}
