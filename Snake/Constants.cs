﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    static class Constants
    {
        public static int SCREEN_WIDTH = 25;
        public static int SCREEN_HEIGHT = 25;
        public static char SNAKE_BODY_SYMBOL = '█';
        public static char BACKGROUND_SYMBOL = ' ';
        public static ConsoleColor BACKGROUND_COLOR = ConsoleColor.Black;
    }
}
