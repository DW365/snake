﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    class Apple
    {
        public Utils.Point Position { get; private set; }
        private Random rnd;
        public Apple()
        {
            rnd = new Random();
            Position = new Utils.Point(rnd.Next(Constants.SCREEN_WIDTH),rnd.Next(Constants.SCREEN_HEIGHT));
        }
        public void Move()
        {
            Position.X = rnd.Next(Constants.SCREEN_WIDTH);
            Position.Y = rnd.Next(Constants.SCREEN_HEIGHT);
        }
        public void Draw()
        {
            Console.SetCursorPosition(Position.X, Position.Y);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("█");
        }
    }
}
